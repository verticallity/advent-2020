#include <stddef.h>
#include <stdlib.h>
#include <stdio.h>

#include "lib/vector.h"



const int DESIRED_SUM = 2020;

int main()
{
	// strictly ordered set of integers inputted
	struct IntVector numbers = ivec_init();
	
	int cur;
continueouter:
	while (scanf("%d", &cur) == 1) {
		if (numbers.array.size == 0) {
			ivec_insert(&numbers, 0, cur);
			continue;
		}
		
		// binary search insertion
		size_t imin = 0;
		size_t imax = numbers.array.size;
		while (imin < imax) {
			size_t index = (imin + imax) >> 1;
			int at_index = numbers.array.data[index];
			
			if (cur == at_index) {
				goto continueouter;
			}
			
			if (cur < at_index) {
				imax = index;
			} else if (cur > at_index) {
				imin = index + 1;
			}
		}
		ivec_insert(&numbers, imin, cur);
	}
	
	int* data = numbers.array.data;
	
	{
		int i1 = 0;
		int i2 = numbers.array.size - 1;
		while (i1 < i2) {
			int sum = data[i1] + data[i2];
			if (sum < DESIRED_SUM) {
				++i1;
			} else if (sum > DESIRED_SUM) {
				--i2;
			} else {
				printf("%d×%d = %d\n",
					data[i1],
					data[i2],
					data[i1] * data[i2]);
				break;
			}
		}
	}
	
	// okay time for the sets of three oh boy oh gee
	for (int ibase = 0; ibase < numbers.array.size - 2; ++ibase) {
		int required_sum = DESIRED_SUM - data[ibase];
		int i1 = ibase + 1;
		int i2 = numbers.array.size - 1;
		while (i1 < i2) {
			int sum = data[i1] + data[i2];
			if (sum < required_sum) {
				++i1;
			} else if (sum > required_sum) {
				--i2;
			} else {
				printf("%d×%d×%d = %d\n",
					data[ibase],
					data[i1],
					data[i2],
					data[ibase] * data[i1] * data[i2]);
				goto end3;
			}
		}
	}
end3:
	
	ivec_drop(numbers);
	return 0;
}
