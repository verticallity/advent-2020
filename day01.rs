const DESIRED_SUM: u32 = 2020;

fn find_sum<'a, I>(mut it: I, total: u32) -> Option<[u32; 2]>
where I: DoubleEndedIterator<Item = &'a u32> {
	let mut left = *it.next()?;
	let mut right = *it.next_back()?;
	loop {
		let cur_sum = left + right;
		if cur_sum < total {
			left = *it.next()?;
		} else if cur_sum > total {
			right = *it.next_back()?;
		} else {
			return Some([left, right]);
		}
	}
}

fn main() {
	use std::io::BufRead;
	let numbers: Result<Result<Vec<u32>, _>, _> =
		std::io::stdin().lock().lines()
		.map(|r| r.map(|s| s.parse()))
		.collect();
	
	let mut numbers = numbers.unwrap().unwrap();
	println!();
	
	numbers.sort();
	
	
	let factors = find_sum(numbers.iter(), DESIRED_SUM).unwrap();
	println!("{}×{} = {}",
		factors[0],
		factors[1],
		factors[0] * factors[1]
	);
	
	let factors = {
		let mut it_start = numbers.iter();
		loop {
			let f1 = *it_start.next().unwrap();
			let total = DESIRED_SUM - f1;
			if let Some([f2, f3]) = find_sum(it_start.clone(), total) {
				break [f1, f2, f3];
			}
		}
	};
	
	println!("{}×{}×{} = {}",
		factors[0],
		factors[1],
		factors[2],
		factors[0] * factors[1] * factors[2],
	);
}
