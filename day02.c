#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

bool sled_password_is_valid(unsigned min, unsigned max, char ch, const char* str)
{
	unsigned count = 0;
	
	for (int i = 0; str[i] != '\0'; ++i) {
		if (str[i] == ch) ++count;
	}
	
	return count >= min && count <= max;
}

bool toboggan_password_is_valid(unsigned i1, unsigned i2, char ch, const char* str)
{
	return (str[i1 - 1] == ch) ^ (str[i2 - 1] == ch);
}

int main()
{
	unsigned sled_count = 0;
	unsigned toboggan_count = 0;
	
	unsigned min, max;
	char counting;
	char* password = malloc(32);
	while (scanf("%u-%u %c: %s", &min, &max, &counting, password) == 4) {
		if (sled_password_is_valid(min, max, counting, password)) ++sled_count;
		if (toboggan_password_is_valid(min, max, counting, password)) ++toboggan_count;
	}
	
	free(password);
	
	printf("\n%u\n%u\n", sled_count, toboggan_count);
	return 0;
}
