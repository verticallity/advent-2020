// this one is poorly done hhhhh
// whatever works well enough

#include <stddef.h>
#include <stdio.h>

const size_t KNOWN_WIDTH = 31;

typedef struct Offset {
	int x;
	unsigned y;
} Offset;

int main()
{
	char line[KNOWN_WIDTH + 1];
	
	const size_t NUM_WAYS = 5;
	Offset offsets[] = {
		{1, 1},
		{3, 1},
		{5, 1},
		{7, 1},
		{1, 2},
	};
	
	size_t xs[NUM_WAYS];
	unsigned lines_til_next[NUM_WAYS];
	unsigned counts[NUM_WAYS];
	
	for (size_t i = 0; i < NUM_WAYS; ++i) {
		xs[i] = lines_til_next[i] = counts[i] = 0;
	}
	
	
	while (scanf("%s", line) == 1) {
		for (size_t i = 0; i < NUM_WAYS; ++i) {
			if (lines_til_next[i] == 0) {
				if (line[xs[i]] == '#') ++counts[i];
				xs[i] = (xs[i] + offsets[i].x) % KNOWN_WIDTH;
				lines_til_next[i] = offsets[i].y;
			}
			--lines_til_next[i];
		}
	}
	
	printf("\n");
	
	unsigned mult = 1;
	for (size_t i = 0; i < NUM_WAYS; ++i) {
		printf("+(%d, %u): %u trees seen\n",
			offsets[i].x,
			offsets[i].y,
			counts[i]
		);
		mult *= counts[i];
	}
	
	printf("\nΠ → %u\n", mult);
	return 0;
}
