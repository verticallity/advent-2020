#pragma once

#include <stddef.h>
#include <stdlib.h>


// dynamically sized array with no claim to or against ownership
// mostly for passing sized buffers to functions, etc
typedef struct IntArray {
	int* data;
	size_t size;
} IntArray;

IntArray iarr_init_capacity(size_t size, size_t alloc_size)
{
	IntArray arr = {
		.data = malloc(sizeof(int) * alloc_size),
		.size = size,
	};
	return arr;
}
IntArray iarr_init(size_t size)
{
	return iarr_init_capacity(size, size);
}

void iarr_drop(IntArray arr)
{
	free(arr.data);
}
