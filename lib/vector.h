#pragma once

#include <assert.h>
#include <stddef.h>
#include <string.h>

#include "array.h"


// mutably sized array
typedef struct IntVector {
	struct IntArray array;
	size_t capacity;
} IntVector;

const size_t VECTOR_DEFAULT_SIZE = 16;

IntVector ivec_init()
{
	struct IntVector vec = {
		.array = iarr_init_capacity(0, VECTOR_DEFAULT_SIZE),
		.capacity = VECTOR_DEFAULT_SIZE,
	};
	return vec;
}
void ivec_drop(IntVector vec)
{
	iarr_drop(vec.array);
}

void ivec_insert(IntVector* vec, size_t loc, int val)
{
	assert(loc <= vec->array.size);
	
	if (vec->capacity == vec->array.size) {
		vec->capacity <<= 1;
		if (vec->capacity == 0) vec->capacity = 1;
		IntArray new_arr = iarr_init_capacity(vec->array.size + 1, vec->capacity);
		
		memcpy(
			new_arr.data,
			vec->array.data,
			sizeof(int) * loc
		);
		new_arr.data[loc] = val;
		memcpy(
			new_arr.data + loc + 1,
			vec->array.data + loc,
			sizeof(int) * (vec->array.size - loc)
		);
		
		iarr_drop(vec->array);
		vec->array = new_arr;
	} else {
		vec->array.size += 1;
		memmove(
			vec->array.data + loc + 1,
			vec->array.data + loc,
			sizeof(int) * (vec->array.size - loc)
		);
		vec->array.data[loc] = val;
	}
}
